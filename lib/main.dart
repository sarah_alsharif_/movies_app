import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'services/database_provider.dart';
import 'my_app.dart';

void main() {
  runApp(
      ChangeNotifierProvider<DatabaseProvider>(
      create: (context)=> DatabaseProvider.getInstance(),
      child: const MyApp()
      )
  );
}



