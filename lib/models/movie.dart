class Movie {
  String title;
  String imdbID;
  String image;
  String year;
  String type;

  Movie({this.title, this.image, this.year, this.imdbID, this.type});

  factory Movie.fromJson(dynamic json) {
    return Movie(
        title: json['Title'],
        image: json['Poster'],
        year: json['Year'].toString(),
        type: json['Type'],
        imdbID: json['imdbID']
    );
  }

  factory Movie.fromMap(Map<String, dynamic> data) {
    return Movie(
        title: data['title'],
        image: data['image'],
        year: data['year'].toString(),
        type: data['type'],
        imdbID: data['imdbID']
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'image': image,
      'year': year,
      'type': type,
      'imdbID': imdbID
    };
  }
}
