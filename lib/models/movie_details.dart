class MovieDetails {
  String imdbID;
  String title;
  String year;
  String imdbRating;
  String genre;
  String writer;
  String director;
  String actors;
  String shortPlot;
  String image;

  MovieDetails({this.imdbID, this.title, this.year, this.imdbRating, this.genre, this.writer, this.director, this.actors, this.shortPlot, this.image});

  factory MovieDetails.fromJson(dynamic json) {
    return MovieDetails(
        title: json['Title'],
        year: json['Year'],
        imdbRating: json['imdbRating'],
        genre: json['Genre'],
        writer: json['Writer'],
        director: json['Director'],
        actors: json['Actors'],
        shortPlot: json['Plot'],
        image: json['Poster'],
    );
  }
}
