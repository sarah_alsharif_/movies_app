import 'package:flutter/material.dart';
import 'package:flutter_app_listview_builder/pages/search_watch_list.dart';
import 'package:flutter_app_listview_builder/pages/watch_list.dart';
import 'package:google_fonts/google_fonts.dart';
import 'pages/search_movies.dart';
import 'pages/categories.dart';

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: GoogleFonts.raleway().fontFamily,
        dividerTheme: DividerThemeData(
          color: Colors.grey[300],
          thickness: 2,
        ),
      ),
      title: 'Movies App',
      initialRoute: '/search',
      routes: {
        '/search': (context) => const SearchMovies(),
        '/watch-list': (context) => const WatchList(),
        '/search-watch-list': (context) => const SearchWatchList(),
        '/categories': (context) => const CategoriesPage(),
      },
    );
  }
}