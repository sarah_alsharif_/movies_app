import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_listview_builder/models/movie_details.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import '../services/database_provider.dart';
import '../widgets/pages_drawer.dart';
import '../models/movie.dart';
import '../widgets/movie_list_item.dart';

class CategoriesPage extends StatefulWidget {
  const CategoriesPage({Key key}) : super(key: key);

  @override
  State<CategoriesPage> createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage> {
  bool initResults = true;
  List<String> categories = [];
  String dropdownValue ;
  List<MovieDetails> categoryMovies = [];
  Future<List<MovieDetails>> futureMoviesDetails;

  @override
  initState() {
    super.initState();
    futureMoviesDetails = DatabaseProvider
        .getInstance()
        .moviesDetails;
  }

  List<MovieDetails> categoryMoviesList(
      String category, List<MovieDetails> watchList) {

      categoryMovies = watchList.where((movie) {
        List<String> genre = movie.genre.split(", ");
        for (var g in genre) {
          if (g.toLowerCase().trim().contains(category.toLowerCase())) {
            return true;
          }
        }
        return false;
      }).toList();

    return categoryMovies.toSet().toList();
  }

  getAllCategories(List<MovieDetails> moviesDetails) {
    for(MovieDetails m in moviesDetails){
      List<String> genre = m.genre.split(",");
        for(String g in genre){
          if(!categories.contains(g)) {
            categories.add(g.trim());
          }
        }
    }
    categories.sort();
    return categories;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF6C0303),
        centerTitle: true,
        title: const Text('Categories'),
      ),
      drawer: Drawer(
          child: PagesDrawer(
            selectedTile: 3,
          )),
      body: Consumer<DatabaseProvider>(
        builder: (context, database, child) {
          return FutureBuilder(
              future: Future.wait([database.movies, database.moviesDetails]),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<Movie> movies = snapshot.data[0];
                  List<MovieDetails> moviesDetails = snapshot.data[1];
                  categories = getAllCategories(moviesDetails).toSet().toList();
                  if(initResults){
                    categoryMovies = moviesDetails;
                    initResults = false;
                  }
                  if (movies.isNotEmpty) {
                    return Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(top: 15.0),
                              child: Text('Choose Category:  ',
                                  style: TextStyle(fontSize: 17)),
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                  right: 15.0, left: 15.0),
                              margin: const EdgeInsets.only(top: 15),
                              decoration: BoxDecoration(
                                  color: const Color(0x5C631111),
                                  borderRadius: BorderRadius.circular(5)
                              ),
                              child: DropdownButtonHideUnderline(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 11.0),
                                  child: DropdownButton<String>(
                                    value: dropdownValue,
                                    alignment: Alignment.center,
                                    hint: const Text('All'),
                                    icon: const Icon(
                                        Icons.arrow_drop_down_rounded),
                                    elevation: 16,
                                    style:
                                    const TextStyle(
                                        color: Colors.black, fontSize: 18),
                                    onChanged: (String newValue) {
                                      setState(() {
                                        dropdownValue = newValue;
                                        categoryMoviesList(dropdownValue,moviesDetails);
                                      });
                                    },
                                    items: categories.toSet().map<DropdownMenuItem<String>>((
                                        String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Expanded(
                          child: categoryMovies.isNotEmpty ? ListView.builder(
                            itemCount: categoryMovies.length,
                            itemBuilder: (BuildContext context, int index) {
                              if((movies.where((element) => element.imdbID == categoryMovies[index].imdbID)).isNotEmpty) {
                                return (MovieItem(
                                  movie: movies
                                      .where((element) =>
                                  element.imdbID ==
                                      categoryMovies[index].imdbID)
                                      .toList()[0]));
                              } else {
                                return const SizedBox(height:0);
                              }
                            },
                          ) : const Center(
                            child: Text("There's no match for your choice",
                              style: TextStyle(
                                  fontSize: 20, fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ],
                    );
                  } else {
                    return const Center(
                      child: Text(
                        "Your watch list is empty!",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                } else if (snapshot.hasError) {
                  return Center(
                      child: Text(
                        snapshot.error.toString(),
                        style: const TextStyle(
                            fontSize: 20, fontWeight: FontWeight.bold),
                      ));
                }
                return const Center(child: CircularProgressIndicator());
              });
        },
      ),
    );
  }
}
