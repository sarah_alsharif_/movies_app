import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../widgets/movie_list_item.dart';
import '../models/movie.dart';

class MoviesList extends StatelessWidget {
  final String query;

  const MoviesList({Key key, this.query}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF6C0303),
        title: const Text('Search results'),
      ),
      body:ListDisplay(query: query),
    );
  }
}

class ListDisplay extends StatefulWidget {
  final String query;

  const ListDisplay({Key key, this.query}) : super(key: key);

  @override
  _ListDisplayState createState() {
    return _ListDisplayState();
  }
}

class _ListDisplayState extends State<ListDisplay> {
  Future<List<Movie>> futureMovies;

  Future<List<Movie>> fetchMovies() async {
    final http.Response response = await http
        .get("https://www.omdbapi.com/?s=${widget.query}&apikey=bb716acc");
    if (jsonDecode(response.body)['Response'] == 'False') {
      if (jsonDecode(response.body)['Error'] == 'Too many results.' ||
          jsonDecode(response.body)['Error'] == 'Incorrect IMDb ID.') {
        throw ("Please enter at least 3 letters!");
      } else {
        throw (jsonDecode(response.body)['Error']);
      }
    } else if (response.statusCode == 200) {
      // success, parse json data
      List jsonArray = jsonDecode(response.body)['Search'];
      List<Movie> movies = jsonArray.map((x) => Movie.fromJson(x)).toList();
      return movies;
    } else {
      throw ("Failed to load data");
    }
  }

  @override
  void initState() {
    super.initState();
    futureMovies = fetchMovies();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: futureMovies,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            List<Movie> movies = snapshot.data;
            return ListView.builder(
              itemCount: movies.length,
              itemBuilder: (BuildContext context, int index) {
                return MovieItem(movie: movies[index]);
              },
            );
          } else if (snapshot.hasError) {
            return Center(
                child: Text(
                  snapshot.error,
                  style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                ));
          }
          return const Center(child: CircularProgressIndicator());
        });
  }
}