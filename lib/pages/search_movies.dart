import 'package:flutter/material.dart';
import 'movies_list.dart';
import '../widgets/pages_drawer.dart';

class SearchMovies extends StatefulWidget {
  const SearchMovies({Key key}) : super(key: key);

  @override
  _SearchMoviesState createState() {
    return _SearchMoviesState();
  }
}

class _SearchMoviesState extends State<SearchMovies> {
  final myController = TextEditingController();


  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        backgroundColor: const Color(0xFF6C0303),
        centerTitle: true,
        title: const Text('Movies App'),
      ),
      drawer: Drawer(
        child: PagesDrawer(selectedTile: 0,),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/images/background.jpg"),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(15, 100, 15, 15),
          child: TextField(
            style: const TextStyle(
              fontSize: 25,
            ),
            cursorColor: const Color(0xFF6C0303),
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              suffixIcon: Container(
                padding: const EdgeInsets.all(5),
                margin: const EdgeInsets.all(12),
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Color(0xFF6C0303),
                ),
                child: IconButton(
                  icon: const Icon(
                    Icons.search,
                    color: Colors.white,
                    size: 30,
                  ),
                  splashColor: Colors.grey,
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MoviesList(
                            query: myController.text.trim(),
                          )),
                    );
                  },
                ),
              ),
              hintText: 'Search for a movie',
              enabledBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 1.0),
              ),
              focusedBorder: const OutlineInputBorder(
                borderSide: BorderSide(color: Colors.white, width: 1.0),
              ),
            ),
            textAlign: TextAlign.center,
            controller: myController,
          ),
        ),
      ),
    );
  }
}