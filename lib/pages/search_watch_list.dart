import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_listview_builder/models/movie_details.dart';
import 'package:provider/provider.dart';
import '../services/database_provider.dart';
import '../widgets/pages_drawer.dart';
import '../models/movie.dart';
import '../widgets/movie_list_item.dart';

class SearchWatchList extends StatefulWidget {
  const SearchWatchList({Key key}) : super(key: key);

  @override
  State<SearchWatchList> createState() => _SearchWatchListState();
}

class _SearchWatchListState extends State<SearchWatchList> {
  bool initResults = true;
  String dropdownValue = 'title';
  List<MovieDetails> searchResults = [];
  Future<List<MovieDetails>> futureMoviesDetails;

  @override
  initState() {
    super.initState();
    futureMoviesDetails = DatabaseProvider.getInstance().moviesDetails;
  }

  List<MovieDetails> searchInWatchList(
      String query, String searchCategory, List<MovieDetails> watchList) {
    if (query == '') {
      return watchList;
    }
    query = query.trim();
    if (searchCategory == 'title') {
      searchResults = watchList
          .where((movie) => movie.title.toLowerCase().contains(query))
          .toList();
    } else if (searchCategory == 'year') {
      searchResults = watchList
          .where((movie) => movie.year.contains(query.toString()))
          .toList();
    } else if (searchCategory == 'rating') {
      searchResults =
          watchList.where((movie) => movie.imdbRating.contains(query)).toList();
    } else if (searchCategory == 'genre') {
      searchResults = watchList.where((movie) {
        List<String> genre = movie.genre.split(",");
        for (var g in genre) {
          if (g.toLowerCase().contains(query.toLowerCase())) {
            return true;
          }
        }
        return false;
      }).toList();
    }
    return searchResults;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF6C0303),
        centerTitle: true,
        title: const Text('Search in your watch list'),
      ),
      drawer: Drawer(
          child: PagesDrawer(
        selectedTile: 2,
      )),
      body: Consumer<DatabaseProvider>(
        builder: (context, database, child) {
          return FutureBuilder(
              future: Future.wait([database.movies, database.moviesDetails]),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  List<Movie> movies = snapshot.data[0];
                  List<MovieDetails> moviesDetails = snapshot.data[1];
                  if (initResults) {
                    searchResults = moviesDetails;
                    initResults = false;
                  }
                  if (movies.isNotEmpty) {
                    return Column(
                      children: [
                        const SizedBox(height: 10),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: TextField(
                            style: const TextStyle(fontSize: 20),
                            cursorColor: const Color(0xFF6C0303),
                            onChanged: (value) {
                              setState(() {
                                searchResults = searchInWatchList(
                                    value, dropdownValue, moviesDetails);
                              });
                            },
                            decoration: const InputDecoration(
                              focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(
                                      width: 2, color: Color(0xFF6C0303))),
                              focusColor: Color(0xFF6C0303),
                              hintText: 'search',
                              suffixIcon: Icon(
                                Icons.search,
                                size: 30,
                                color: Color(0xFF6C0303),
                              ),
                            ),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            const Padding(
                              padding: EdgeInsets.only(top: 15.0),
                              child: Text('Search by:  ',
                                  style: TextStyle(fontSize: 17)),
                            ),
                            Container(
                              padding: const EdgeInsets.only(
                                  right: 15.0, left: 15.0),
                              margin: const EdgeInsets.only(top: 15),
                              decoration: BoxDecoration(
                                  color: const Color(0x5C631111),
                                  borderRadius: BorderRadius.circular(5)),
                              child: DropdownButtonHideUnderline(
                                child: Padding(
                                  padding: const EdgeInsets.only(left: 11.0),
                                  child: DropdownButton<String>(
                                    value: dropdownValue,
                                    icon: const Icon(
                                        Icons.arrow_drop_down_rounded),
                                    elevation: 16,
                                    style: const TextStyle(
                                        color: Colors.black, fontSize: 20),
                                    onChanged: (String newValue) {
                                      setState(() {
                                        dropdownValue = newValue;
                                      });
                                    },
                                    items: <String>[
                                      'title',
                                      'year',
                                      'genre',
                                      'rating'
                                    ].map<DropdownMenuItem<String>>(
                                        (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 10,
                        ),
                        Expanded(
                          child: searchResults.isNotEmpty
                              ? ListView.builder(
                                  itemCount: searchResults.length,
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    if ((movies.where((element) =>
                                            element.imdbID ==
                                            searchResults[index].imdbID))
                                        .isNotEmpty) {
                                      return MovieItem(
                                          movie: movies
                                              .where((element) =>
                                                  element.imdbID ==
                                                  searchResults[index].imdbID)
                                              .toList()[0]);
                                    } else {
                                      return const SizedBox(height: 0);
                                    }
                                  },
                                )
                              : const Center(
                                  child: Text(
                                    "There's no match for your results",
                                    style: TextStyle(
                                        fontSize: 20,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                        ),
                      ],
                    );
                  } else {
                    return const Center(
                      child: Text(
                        "Your watch list is empty!",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 25, fontWeight: FontWeight.bold),
                      ),
                    );
                  }
                } else if (snapshot.hasError) {
                  return Center(
                      child: Text(
                    snapshot.error.toString(),
                    style: const TextStyle(
                        fontSize: 20, fontWeight: FontWeight.bold),
                  ));
                }
                return const Center(child: CircularProgressIndicator());
              });
        },
      ),
    );
  }
}
