import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../services/database_provider.dart';
import '../widgets/pages_drawer.dart';
import '../models/movie.dart';
import '../widgets/movie_list_item.dart';

class WatchList extends StatelessWidget {

  const WatchList({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: const Color(0xFF6C0303),
        centerTitle: true,
        title: const Text('My Watch List'),
      ),
      drawer:Drawer(
          child: PagesDrawer(selectedTile: 1,)),
      body: Consumer<DatabaseProvider>(
        builder: (context, database, child){
          return FutureBuilder<List<Movie>>(
                future: database.movies,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    List<Movie> movies = snapshot.data;
                    if (movies.isNotEmpty) {
                      return ListView.builder(
                        itemCount: movies.length,
                        itemBuilder: (BuildContext context, int index) {
                          return MovieItem(movie: movies[index]);
                        },
                      );
                    } else {
                      return const Center(
                          child: Text(
                            "Your watch list is empty!",
                            textAlign: TextAlign.center,
                            style:
                            TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                          ),
                        );
                    }
                  } else if (snapshot.hasError) {
                    return Center(
                        child: Text(
                          snapshot.error.toString(),
                          style: const TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold),
                        ));
                  }
                  return const Center(child: CircularProgressIndicator());
                });
        },
      ),
    );
  }
}
