import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter_app_listview_builder/models/movie_details.dart';
import 'package:http/http.dart' as http;
import '../models/movie.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider extends ChangeNotifier{

  DatabaseProvider._internal();
  static final DatabaseProvider _db = DatabaseProvider._internal();

  factory DatabaseProvider.getInstance(){
    return _db;
  }

  static const int version = 1;
  static  Database _database;

  static const String tableName = 'movies';
  Future<Database> get database async {
    return _database ??= await initDB();
  }

  Future<Database> initDB() async {
    String path = await getDatabasesPath();
    path += 'movies.db';
    return await openDatabase(
      path,
      version: version,
      onCreate: (db, version) async {
        await db.execute('''
          create table $tableName (
            imdbID text primary key,
            title text not null,
            image text,
            year text,
            type text
          )
          ''');
      },
        onUpgrade: (Database db, int oldVersion, int newVersion) async{
          await db.execute('''
          create table $tableName (
            imdbID text primary key,
            title text not null,
            image text,
            year text,
            type text
          )
          ''');
        }
    );
  }
  Future<bool> isExist(String imdbID) async{
    List<Movie> watchList = await movies;
    for(var m in watchList){
      if(m.imdbID == imdbID){
        return true;
      }
    }
    return false;

  }

  Future<List<Movie>> get movies async {
    final db = await database;
    List<Map<String, dynamic>> result = await db.query(tableName, orderBy: 'title asc');
    List<Movie> movies = [];
    for (var value in result) {
      movies.add(Movie.fromMap(value));
    }
    return movies;
  }

  insert(Movie movie) async {
    final db = await database;
    await db.insert(tableName, movie.toMap());
    notifyListeners();
  }

  removeAll() async {
    final db = await database;
    await db.delete(tableName);
    notifyListeners();
  }

  removeMovie(String imdbID) async {
    final db = await database;
    await db.delete(tableName, where: 'imdbID=?', whereArgs: [imdbID]);
    notifyListeners();
  }

  Future<List<MovieDetails>> get moviesDetails async {
    final db = await database;
    List<Map<String, dynamic>> result = await db.query(tableName, orderBy: 'title asc');
    List<Movie> movies = [];
    for (var value in result) {
      movies.add(Movie.fromMap(value));
    }
    List<MovieDetails> watchListMovies = [];
    for(Movie m in movies){
      final http.Response response = await http.get("https://www.omdbapi.com/?i=${m.imdbID}&apikey=bb716acc");
      if (jsonDecode(response.body)['Response'] == 'False') {
        throw (jsonDecode(response.body)['Error']);
      } else if (response.statusCode == 200) {
        // success, parse json data
        MovieDetails movie = MovieDetails.fromJson(jsonDecode(response.body));
        movie.imdbID = m.imdbID;
        watchListMovies.add(movie);
      } else {
        throw ("Failed to load data");
      }
    }

    return watchListMovies;

  }


}