import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../widgets/movie_details_item.dart';
import '../models/movie_details.dart';

class FetchMovieDetails extends StatefulWidget {
  final String imdbID;

  const FetchMovieDetails({Key key, this.imdbID}) : super(key: key);

  @override
  State<FetchMovieDetails> createState() => _MovieDetailsState();
}

class _MovieDetailsState extends State<FetchMovieDetails> {
  Future<MovieDetails> futureMovieDetails;
  Future<String> futureFullPlot;

  Future<MovieDetails> fetchMovieDetails() async {
    final http.Response response = await http
        .get("https://www.omdbapi.com/?i=${widget.imdbID}&apikey=bb716acc");
    if (jsonDecode(response.body)['Response'] == 'False') {
      throw (jsonDecode(response.body)['Error']);
    } else if (response.statusCode == 200) {
      // success, parse json data
      MovieDetails movie = MovieDetails.fromJson(jsonDecode(response.body));
      movie.imdbID = widget.imdbID;
      return movie;
    } else {
      throw ("Failed to load data");
    }
  }

  Future<String> fetchMovieFullPlot() async {
    final http.Response response = await http.get(
        "https://www.omdbapi.com/?i=${widget.imdbID}&plot=full&apikey=bb716acc");
    if (jsonDecode(response.body)['Response'] == 'False') {
      throw (jsonDecode(response.body)['Error']);
    } else if (response.statusCode == 200) {
      // success, parse json data
      String fullPlot = jsonDecode(response.body)['Plot'];
      return fullPlot;
    } else {
      throw ("Failed to load data");
    }
  }

  @override
  void initState() {
    super.initState();
    futureMovieDetails = fetchMovieDetails();
    futureFullPlot = fetchMovieFullPlot();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: const Color(0xFF6C0303),
        title: const Text('Movie Details'),
      ),
      body: FutureBuilder(
          future: Future.wait([futureMovieDetails, futureFullPlot]),
          builder: (context, AsyncSnapshot<List<dynamic>> snapshot) {
            if (snapshot.hasData) {
              MovieDetails movie = snapshot.data[0];
              String fullPlot = (snapshot.data[1] != 'N/A'
                  ? snapshot.data[1]
                  : 'Not available !');
              return MovieDetailsItem(
                movie: movie,
                fullPlot: fullPlot,
              );
            } else if (snapshot.hasError) {
              return Center(
                  child: Text(
                    snapshot.error,
                    style: const TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  ));
            }
            return const Center(child: CircularProgressIndicator());
          }),
    );
  }
}
