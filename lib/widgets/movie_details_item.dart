import 'package:flutter/material.dart';
import '../models/movie_details.dart';

class MovieDetailsItem extends StatefulWidget {
  final MovieDetails movie;
  final String fullPlot;

  const MovieDetailsItem({Key key, this.movie, this.fullPlot})
      : super(key: key);

  @override
  State<MovieDetailsItem> createState() => _MovieDetailsItemState();
}

class _MovieDetailsItemState extends State<MovieDetailsItem> {
  bool _plotIsShort = true;

  void _togglePlot() {
    setState(() {
      _plotIsShort = !_plotIsShort;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(

        child: Column(children: [
          Container(
            margin: const EdgeInsets.all(15.0),
            padding: const EdgeInsets.all(15.0),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
              width: 2,
              color: Colors.red[900],
            ))),
            child: Text(
              widget.movie.title +
                  " (" +
                  (widget.movie.year != 'N/A'
                      ? widget.movie.year
                      : 'Not available !') +
                  ")",
              style: const TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.center,
            ),
          ),
          SizedBox(
            child: Image.network(
              widget.movie.image,
              fit: BoxFit.cover,
              errorBuilder: (context, error, stackTrace) {
                return const SizedBox(
                  child: Image(image: AssetImage("./assets/images/invalid.png")),
                );
              },
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(22.0),
            child: Column(children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Genre : ",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      Flexible(
                        child: Text(
                            (widget.movie.genre != 'N/A'
                                ? widget.movie.genre
                                : 'Not available !'),
                            style: const TextStyle(
                              fontSize: 16,
                            )),
                      ),
                    ]),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Rating: ",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      Text(
                          (widget.movie.imdbRating != 'N/A'
                              ? widget.movie.imdbRating
                              : 'Unrated'),
                          style: const TextStyle(
                            fontSize: 16,
                          )),
                    ]),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("Actors: ",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      Flexible(
                        child: Text(
                            (widget.movie.actors != 'N/A'
                                ? widget.movie.actors
                                : 'Not available !'),
                            style: const TextStyle(
                              fontSize: 16,
                            )),
                      ),
                    ]),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("writers: ",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      Flexible(
                        child: Text(
                            (widget.movie.writer != 'N/A'
                                ? widget.movie.writer
                                : 'Not available !'),
                            style: const TextStyle(
                              fontSize: 16,
                            )),
                      )
                    ]),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Text("director: ",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.bold)),
                      Flexible(
                        child: Text(
                            (widget.movie.director != 'N/A'
                                ? widget.movie.director
                                : 'Not available !'),
                            style: const TextStyle(
                              fontSize: 17,
                            )),
                      )
                    ]),
              ),
            ]),
          ),
          Container(
            padding: const EdgeInsets.all(8),
            decoration: BoxDecoration(
                border: Border(
                    bottom: BorderSide(
              width: 2,
              color: Colors.red[900],
            ))),
            child: const Text(
              "Story: ",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.only(right: 30.0, left: 30.0, top: 15.0),
                child: Text(
                  (widget.movie.shortPlot != 'N/A' && widget.fullPlot != 'N/A' )
                      ? (_plotIsShort
                          ? (widget.movie.shortPlot != 'N/A'
                              ? widget.movie.shortPlot
                              : 'Not available !')
                          : (widget.fullPlot != 'N/A'
                              ? widget.fullPlot
                              : 'Not available !'))
                      : (widget.movie.shortPlot != 'N/A'
                          ? widget.movie.shortPlot
                          : (widget.fullPlot != 'N/A'
                              ? widget.fullPlot
                              : 'Not available !')),
                  textAlign: TextAlign.justify,
                  style: const TextStyle(fontSize: 17, height: 2),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: 10, bottom: 50.0, right: 30.0),
                child: InkWell(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Text(
                        (widget.movie.shortPlot != 'N/A' &&
                                widget.fullPlot != 'N/A' && widget.movie.shortPlot != widget.fullPlot)
                            ? (_plotIsShort ? "[show more]" : "[show less]")
                            : (""),
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            color: Colors.red[900]),
                      ),
                    ],
                  ),
                  onTap: () {
                    _togglePlot();
                  },
                ),
              ),
            ],
          ),
        ]),
      );
  }
}
