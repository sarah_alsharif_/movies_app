import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../services/database_provider.dart';
import '../services/fetch_movie_details.dart';
import '../models/movie.dart';

class MovieItem extends StatelessWidget {
  final Movie movie;

  const MovieItem({@required this.movie, Key key}) : super(key: key);

  bool _isExistInWatchList(List<Movie> wl) {
    for (var m in wl) {
      if (m.imdbID == movie.imdbID) {
        return true;
      }
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(4),
      height: 150,
      child: GestureDetector(
        onTap: () => {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (BuildContext context) => FetchMovieDetails(
                  imdbID: movie.imdbID,
                ),
              )),
        },
        child: Card(
          child: Row(
            children: [
            SizedBox(
            height: 150,
            width: 90,
            child: Image.network(
              movie.image,
              fit: BoxFit.cover,
              errorBuilder: (context, error, stackTrace) {
                return const SizedBox(
                  height: 150,
                  width: 90,
                  child: Image(image: AssetImage("./assets/images/invalid.png")),
                );
              },
            ),
          ),
              Expanded(
                  child: Container(
                padding: const EdgeInsets.fromLTRB(20, 5, 5, 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      (movie.title.length < 28
                          ? movie.title
                          : "${movie.title.substring(0, 28)}..."),
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 18),
                    ),
                    Text(movie.type, style: const TextStyle(fontSize: 16)),
                    Row(
                      children: [
                        Consumer<DatabaseProvider>(
                          builder: (context, database, child) {
                            return FutureBuilder<List<Movie>>(
                                future: DatabaseProvider.getInstance().movies,
                                builder: (context, snapshot) {
                                  if (snapshot.hasData) {
                                    List<Movie> movies = snapshot.data;
                                    return IconButton(
                                        icon: Icon(
                                            _isExistInWatchList(movies)
                                                ? Icons.access_time_filled
                                                : Icons.access_time,
                                            color: Colors.black),
                                        onPressed: () {
                                          _isExistInWatchList(movies)
                                              ? database
                                                  .removeMovie(movie.imdbID)
                                              : database.insert(movie);
                                        });
                                  } else if (snapshot.hasError) {
                                    return Center(
                                        child: Text(
                                      snapshot.error.toString(),
                                      style: const TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.bold),
                                    ));
                                  }
                                  return const Center(
                                      child: CircularProgressIndicator());
                                });
                          },
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.centerRight,
                            child: Text(movie.year,
                                style: const TextStyle(fontSize: 16)),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ))
            ],
          ),
        ),
      ),
    );
  }
}
