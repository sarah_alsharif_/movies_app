import 'package:flutter/material.dart';

class PagesDrawer extends StatefulWidget {
  int selectedTile;

  PagesDrawer({Key key, this.selectedTile}) : super(key: key);

  @override
  State<PagesDrawer> createState() => _PagesDrawerState();
}

class _PagesDrawerState extends State<PagesDrawer> {


  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: ListView(
        children: [
          const SizedBox(
            height: 150,
            child: DrawerHeader(
              decoration: BoxDecoration(
                color: Color(0xff450000),
              ),
              child: Center(
                child: Text('Movies App',
                    style: TextStyle(
                        fontSize: 25,
                        color: Colors.white,
                        fontWeight: FontWeight.w600)),
              ),
            ),
          ),
          buildMenuItem(
            icon: Icons.search,
            title: "Search for movies",
            context: context,
            route: '/search',
            index: 0,
          ),
          const Divider(),
          buildMenuItem(
            icon: Icons.watch_later,
            title: "My watch list",
            context: context,
            route: '/watch-list',
            index: 1,
          ),
          const Divider(),
          buildMenuItem(
            icon: Icons.search,
            title: "Search in watch list",
            context: context,
            route: '/search-watch-list',
            index: 2,
          ),
          const Divider(),
          buildMenuItem(
              icon: Icons.category,
              title: "Categories",
              context: context,
              route: '/categories',
              index: 3),
        ],
      ),
    );
  }

  void selectTile(int index) {
    setState(() {
      widget.selectedTile = index;
    });
  }

  void onClick({String route, BuildContext context}) {
    Navigator.pushNamed(context, route);
  }

  buildMenuItem(
      {IconData icon,
      String title,
      String route,
      BuildContext context,
      int index}) {
    return Container(
      margin: const EdgeInsets.only(left: 10, right: 10),
      decoration: BoxDecoration(
        color: widget.selectedTile == index
            ? const Color(0x5C631111)
            : Colors.transparent,
        borderRadius: const BorderRadius.all(Radius.circular(10.0)),
      ),
      child: ListTileTheme(
        selectedColor: const Color(0xFF360000),
        child: ListTile(
          shape: ContinuousRectangleBorder(
            borderRadius: BorderRadius.circular(5),
          ),
          leading: Icon(
            icon,
            size: 30,
            color: widget.selectedTile == index
                ? const Color(0xFF360000)
                : Colors.black45,
          ),
          title: Text(
            title,
            style: const TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
          ),
          onTap: () {
            Navigator.pop(context);
            Navigator.pop(context);
            selectTile(index);
            onClick(context: context, route: route);
          },
          selected: widget.selectedTile == index,
        ),
      ),
    );
  }
}
